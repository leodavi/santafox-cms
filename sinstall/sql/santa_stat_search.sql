DROP TABLE IF EXISTS `%PREFIX%_stat_search`;
-- sqlseparator------------------------------------------------
CREATE TABLE `%PREFIX%_stat_search` (
  `IDSearch` int(10) unsigned NOT NULL auto_increment,
  `search` varchar(32) NOT NULL default '',
  `preg_host` varchar(32) NOT NULL default '',
  `preg_word` varchar(32) NOT NULL default '',
  PRIMARY KEY  (`IDSearch`)
) ENGINE=MyISAM AUTO_INCREMENT=95 DEFAULT CHARSET=utf8 COMMENT='Статистика - поисковые системы';
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('1','Yandex','ya(ndex)?\\.ru','(text|qs=text)');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('2','Aport','aport\\.ru','r=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('3','Rambler','rambler\\.ru','words=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('4','MetaBot','metabot\\.ru','st=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('5','Google (Images)','images\\.google\\.','(p|q)=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('6','Google','google\\.','(p|q)=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('7','MSN','msn\\.','q=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('8','Voila','voila\\.','kw=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('9','Yahoo','yahoo\\.','p=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('10','Lycos','lycos\\.','query=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('11','Alexa','alexa\\.com','q=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('12','AllTheWeb','alltheweb\\.com','q(|uery)=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('13','AltaVista','altavista\\.','q=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('14','DMOZ','dmoz\\.org','search=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('15','Netscape','netscape\\.','search=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('16','Terra','search\\.terra\\.','query=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('17','Search.com','www\\.search\\.com','q=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('18','AOL','search\\.aol\\.co','query=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('19','NorthernLight','northernlight\\.','qr=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('20','Hotbot','hotbot\\.','mt=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('21','Kvasir','kvasir\\.','q=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('22','MetaCrawler (Metamoteur)','metacrawler\\.','general=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('23','Go2Net (Metamoteur)','go2net\\.com','general=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('24','Go.com','(^|\\.)go\\.com','qt=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('25','Euroseek','euroseek\\.','query=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('26','Excite','excite\\.','search=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('27','Looksmart','looksmart\\.','key=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('28','Spray','spray\\.','string=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('29','NBCI','nbci\\.com\\/search','keyword=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('30','Ask Jeeves','(^|\\.)ask\\.com','ask=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('31','Atomz','atomz\\.','sp-q=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('32','Overture','overture\\.com','keywords=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('33','Teoma','teoma\\.','q=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('34','Find Articles','findarticles\\.com','key=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('35','InfoSpace','infospace\\.com','qkw=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('36','Mamma','mamma\\.','query=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('37','Dogpile','dogpile\\.com','qkw=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('38','WISENut','wisenut\\.com','query=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('39','Earth Link','search\\.earthlink\\.net','q=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('40','Sympatico','search\\.sli\\.sympatico\\.ca','query=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('41','i-une.com','i-une\\.com','(keywords|q)=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('42','Cade','engine\\.exe','p1=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('43','Meta Miner','miner\\.bol\\.com\\.br','q=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('44','Baidu','baidu\\.com','word=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('45','Sina','search\\.sina\\.com','word=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('46','Sohu','search\\.sohu\\.com','word=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('47','Atlas.cz','atlas\\.cz','searchtext=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('48','Seznam.cz','seznam\\.cz','w=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('49','Centrum.cz','centrum\\.cz','q=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('50','Najdi.to','najdi\\.to','dotaz=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('51','Opasia','opasia\\.dk','q=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('52','Thor (danielsen.com)','danielsen\\.com','q=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('53','SOL','sol\\.dk','q=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('54','Jubii','jubii\\.dk','soegeord=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('55','Find','find\\.dk','words=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('56','Edderkoppen','edderkoppen\\.dk','query=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('57','Orbis','orbis\\.dk','search_field=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('58','1Klik','1klik\\.dk','query=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('59','Ofir','ofir\\.dk','querytext=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('60','Ilse','ilse\\.','search_for=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('61','Vindex','vindex\\.','in=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('62','Ask Jeeves UK','(^|\\.)ask\\.co\\.uk','ask=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('63','BBC','bbc\\.co\\.uk\\/cgi-bin\\/search','q=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('64','Freeserve','ifind\\.freeserve','q=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('65','Looksmart UK','looksmart\\.co\\.uk','key=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('66','Mirago','mirago\\.','txtsearch=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('67','Splut','splut\\.','pattern=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('68','Spotjockey','spotjockey\\.','Search_Keyword=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('69','UK Directory','ukdirectory\\.','k=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('70','UKIndex','ukindex\\.co\\.uk','stext=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('71','UK Plus','ukplus\\.','search=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('72','searchy.co.uk','searchy\\.co\\.uk','search_term=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('73','Ihmemaa','haku\\.www\\.fi','w=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('74','Francitй','francite\\.','name=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('75','Club-internet','recherche\\.club-internet\\.fr','q=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('76','Fireball','fireball\\.de','q=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('77','Infoseek','infoseek\\.de','qt=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('78','Abacho','suchen\\.abacho\\.de','q=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('79','T-Online','brisbane\\.t-online\\.de','q=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('80','Heureka','heureka\\.hu','heureka=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('81','Origo-Vizsla-Katalуgus','vizsla\\.origo\\.hu\\/katalogus?','q=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('82','Origo-Vizsla','vizsla\\.origo\\.hu','search=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('83','Startlapkeresх','lapkereso\\.hu','keres.php');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('84','Gуliбt','goliat\\.hu','KERESES=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('85','Index','index\\.hu','search.php3');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('86','Wahoo','wahoo\\.hu','q=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('87','FreeWeb','freeweb\\.hu','KERESES=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('88','Internetto Keresх','search\\.internetto\\.hu','searchstr=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('89','Virgilio','virgilio\\.it','qs=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('90','start.no','sok\\.start\\.no','q=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('91','Szukaj','szukaj\\.wp\\.pl','szukaj=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('92','Evreka','evreka\\.passagen\\.se','q=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('93','search.ch','search\\.ch','q=');
-- sqlseparator------------------------------------------------ 
INSERT INTO `%PREFIX%_stat_search` VALUES ('94','search.bluewin.ch','search\\.bluewin\\.ch','qry=');