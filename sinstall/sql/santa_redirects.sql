DROP TABLE IF EXISTS `%PREFIX%_redirects`;
-- sqlseparator------------------------------------------------
CREATE TABLE `%PREFIX%_redirects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL,
  `redirect` varchar(255) NOT NULL,
  `code` int(3) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `url` (`url`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;