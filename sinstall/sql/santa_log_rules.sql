DROP TABLE IF EXISTS `%PREFIX%_log_rules`;
-- sqlseparator------------------------------------------------
CREATE TABLE `%PREFIX%_log_rules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level` enum('ERROR','WARNING','INFO','DEBUG') DEFAULT NULL,
  `log_id` varchar(255) DEFAULT NULL,
  `handler` varchar(255) NOT NULL,
  `params` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_log_rules` (`id`, `level`, `log_id`, `handler`, `params`) VALUES
(1,	NULL,	NULL,	'Log_Debug',	'{\"key\":\"1\"}');