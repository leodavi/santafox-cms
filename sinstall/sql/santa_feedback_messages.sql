DROP TABLE IF EXISTS `%PREFIX%_feedback_messages`;
-- sqlseparator------------------------------------------------
CREATE TABLE IF NOT EXISTS `%PREFIX%_feedback_messages` (
			`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
			`module_id` varchar(255) NOT NULL,
			 `name` varchar(255) NOT NULL,
			 `email` varchar(255) NOT NULL,
			 `theme` varchar(255) NOT NULL,
			`message` text,
			 `pubdate` datetime NOT NULL,
			 `status` enum('success','error') NOT NULL,
			 PRIMARY KEY  (`id`),
			 KEY `module_id` (`module_id`)
		 ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1