DROP TABLE IF EXISTS `%PREFIX%_backup_ignoredtables`;
-- sqlseparator------------------------------------------------
CREATE TABLE `%PREFIX%_backup_ignoredtables` (
  `ruleid` int(5) unsigned default NULL,
  `tablename` varchar(255) NOT NULL,
  UNIQUE KEY `tablename` (`tablename`,`ruleid`),
  KEY `ruleid` (`ruleid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Игнорируемые в бекапе таблицы';
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_backup_ignoredtables` VALUES ('2','%PREFIX%_backup');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_backup_ignoredtables` VALUES ('2','%PREFIX%_backup_ignoredexts');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_backup_ignoredtables` VALUES ('2','%PREFIX%_backup_ignoredpaths');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_backup_ignoredtables` VALUES ('2','%PREFIX%_backup_ignoredtables');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_backup_ignoredtables` VALUES ('2','%PREFIX%_backup_rules');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_backup_ignoredtables` VALUES ('2','%PREFIX%_search1_docs');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_backup_ignoredtables` VALUES ('2','%PREFIX%_search1_ignored');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_backup_ignoredtables` VALUES ('2','%PREFIX%_search1_index');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_backup_ignoredtables` VALUES ('2','%PREFIX%_search1_words');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_backup_ignoredtables` VALUES ('2','%PREFIX%_stat_domain');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_backup_ignoredtables` VALUES ('2','%PREFIX%_stat_host');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_backup_ignoredtables` VALUES ('2','%PREFIX%_stat_index');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_backup_ignoredtables` VALUES ('2','%PREFIX%_stat_partner');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_backup_ignoredtables` VALUES ('2','%PREFIX%_stat_partner_eregs');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_backup_ignoredtables` VALUES ('2','%PREFIX%_stat_referer');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_backup_ignoredtables` VALUES ('2','%PREFIX%_stat_robot');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_backup_ignoredtables` VALUES ('2','%PREFIX%_stat_search');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_backup_ignoredtables` VALUES ('2','%PREFIX%_stat_uri');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_backup_ignoredtables` VALUES ('2','%PREFIX%_stat_word');