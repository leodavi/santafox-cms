<?php
/**
 * @author Ivan Kudryavsky [spam@mr-god.net]
 * @link http://master.virmandy.net
 * @link http://vk.com/id171080259
 *
 * Данный класс является обработчиком логов, отправляющий логи на e-mail
 */
class Log_Email extends LogHandler
{

    public function send($params,$host,$session,$level,$file,$line,$text,$id,$data)
    {

        $log = "<b>Хост:</b> ".$host."<br>";
        $log = "<b>Сессия:</b> ".$session."<br>";
        $log = "<b>Дата:</b> ".date("d.m.Y H:i:s")."<br>";
        $log.= "<b>Уровень:</b> ".$level."<br>";
        $log.= "<b>Файл:</b> ".$file.":".$line."<br>";
        if($id) $log.="<b>ID:</b> #".$id."<br>";
        $log.= "<b>Сообщение:</b><br> ".$text;

        if (count($data)>0){
            $log.= "<br><b>Дополнительные параметры:</brb><br> ";
            foreach($data as $k=>$v){
                $log .= $k.":".$v."<br>";
            }
        }

        $headers  = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        mail($params["email"], $params["subject"], $log, $headers);
    }

    public function get_name($lang)
    {
        return "Отправка лога на e-mail";
    }

    public function get_description($lang)
    {
        return "Данный обработчик отправляет лог на указанный e-mail в письме с указанной темой.";
    }

    public function get_params($lang){
        return array(
            array("id"=>"email","name"=>"E-mail","required"=>true, "description"=>"На данный e-mail будет отправлено письмо"),
            array("id"=>"subject","name"=>"Тема письма:","required"=>true)
        );
    }
}