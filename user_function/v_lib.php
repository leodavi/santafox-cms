<?php
/**
 * Пользовательские функции
 * Пользовательские функции предназначены для вызова функций в шаблоне с передачей параметров и возвратом результата.
 * Для вызова пользовательской функции в шаблоне необходимо прописать такую конструкцию [@function-<название функции>(<передаваемая ей строка>)]
 */

/**
 * Функция v_request($a) предназначена для вывода в шаблона значения из $_REQUEST.
 * Пример вызова [@function-v_request(page)]
 * Если вы параметр $_REQUEST[$name] отсутсвует вы можете вывести значение по умолчанию [@function-v_request(page ||| default_value)]
 * @param string $name
 * @param string $default
 * @return null|string
 */
function v_request($a){
    $a = explode(" ||| ", $a);
    $name = $a[0];
    if(isset($a[1])) $default = $a[1]; else $default = "";
    if(isset($_REQUEST[$name]))
        return $_REQUEST[$name];
    else
        return "".$default;
}

/**
 * Функция v_selected_print($a) предназначена для сохранения состояний выпадающих списков.
 * Пример вызова [@function-v_selected_print(name_select ;;||;; value)]
 * Данную функцию необходимо писать в каждом из <option> - <option value="55" [@function-v_selected_print(name_select ;;||;; 55)]>
 * Выводит ' selected="selected"' если $_REQUEST[$name]==$value
 * @param $name
 * @param $value
 * @return string
 */
function v_selected_print($a){
    $a = explode(" ;;||;; ",$a);
    $name = $a[0];
    if(isset($a[1])) $value = $a[1]; else  return '';
    if(isset($_REQUEST[$name]) && $_REQUEST[$name]==$value) return ' selected="selected"'; else return '';
}

/**
 * Функция v_checked_print($a) предназначена для сохранения состояния чекбоксов и радиокнопок.
 * Пример вызова [@function-v_checked_print(name_сhekbox)].
 * Например, <input type="checkbox" name="my_checkbox" [@function-v_checked_print(my_checkbox)]>
 * Выводит checked="checked" если  существует $_REQUEST[$name]
 * @param $name
 * @return string
 */
function v_checked_print($name){
    if(isset($_REQUEST[$name])) return ' checked="checked"'; else return '';
}

/**
 * Функция, выводящая на экран $text, если $value1 == $value2
 * Пример вызова [@function-v_if_equally(12  ;;||;; [@function-v_request(cid)] ;;||;; Текст, который отобразится)]
 * @param $a
 * @return string
 */
function v_if_equally($a){
    $a = explode(" ;;||;; ",$a);
    $value1 = $a[0];
    if(isset($a[1])) $value2 = $a[1]; else  return '';
    if(isset($a[2])) $text = $a[2]; else  return '';
    if($value1==$value2)
        return $text;
    else
        return "";
}

/**
 * Функция, выводящая на экран $text, если $value1 >= $value2
 * Пример вызова [@function-v_if_larger(5000;;||;;%price_value%;;||;;Скидка 50% на товары, цена которых от 5000 бубликов!])
 * @param $a
 * @return string
 */
function v_if_larger($a){
    $a = explode(" ;;||;; ",$a);
    $value1 = $a[0];
    if(isset($a[1])) $value2 = $a[1]; else  return '';
    if(isset($a[2])) $text = $a[2]; else  return '';
    if($value1>=$value2)
        return $text;
    else
        return "";
}