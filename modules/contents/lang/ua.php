<?php
$type_langauge = 'ua';

$il['contents_modul_base_name'] = 'Блоки контенту';
$il['contents_modul_base_name1'] = 'Блок контенту';

$il['contents_block_add_edit'] = 'Додати/редагувати блок';
$il['contents_block_add'] = 'Додати блок';
$il['contents_block_name'] = 'Заголовок блоку';
$il['contents_block_descr'] = 'Опис блоку';
$il['contents_items_count'] = 'Кількість';

$il['content_fields_field_title'] = 'Назва поля';
$il['content_fields_field_name'] = 'Ідентифікатор';
$il['content_fields_field_type'] = 'Тип поля';
$il['content_fields_field_type_string'] = 'Рядок';
$il['content_fields_field_type_textarea'] = 'Текстове';
$il['content_fields_field_type_editor'] = 'Редактор';
$il['content_fields_field_type_select'] = 'Набір значень (ENUM)';
$il['content_fields_field_type_checkbox'] = 'Набір значень (SET)';
$il['content_fields_field_type_fileselect'] = 'Вибір файлу';
$il['content_fields_field_type_imageselect'] = 'Вибір зображення';
$il['content_fields_field_type_label'] = 'Кожне значення з нового рядка';
$il['content_fields_field_new'] = 'Нове довільне поле';
$il['content_fields_field_edit'] = 'Редагування поля';
$il['content_fields_field_id'] = 'ID';
$il['content_fields_field_date'] = 'Дата додавання';
$il['content_fields_field_order'] = 'Порядок сортування';
$il['content_fields_delete'] = 'Видалити вибрані';
$il['content_block_delete_confirm'] = 'Ви дійсно бажаєте видалити блок контенту?';
$il['content_fields_delete_confirm'] = 'Ви дійсно бажаєте видалити поле контенту?';
$il['content_item_delete_confirm'] = 'Ви дійсно бажаєте видалити вибраний контент?';
$il['content_fields_save'] = 'Зберегти порядок';
$il['content_fields_add'] = 'Додати поле';
$il['content_fields_edit'] = 'Редагувати поле';

$il['contents_menu_block_label'] = 'Основні дії';
$il['contents_menu_blocks'] = 'Блоки контенту';
$il['contents_menu_fields'] = 'Список полів';
$il['content_fields_field_new'] = 'Нове поле контенту';

$il['pub_show_content'] = 'Показати вміст блоку';
$il['contents_template'] = 'Шаблон виведення';
$il['contents_select_blocks'] = 'Блок-контенту';
$il['contents_property_sort_ask'] = 'ID по зростанню';
$il['contents_property_sort_desk'] = 'ID за спаданням';
$il['contents_property_sort_order_num_ask'] = 'Порядок сортування за зростанням';
$il['contents_property_sort_order_num_desk'] = 'Порядок сортування по спадаючій';

$il['contents_list_blocks'] = 'Список блоків контенту';
$il['contents_list_contents'] = 'Вміст блоку';
$il['contents_list_selectall'] = 'Вибрати все';
$il['contents_content'] = 'Короткий вміст';

$il['contents_form_header_add'] = 'Додавання контенту';
$il['contents_form_header_edit'] = 'Редагування контенту';

$il['contents_content_fields_empty'] = '- не вибрано -';
$il['content_block_not_set'] = '- не вибрано -';
$il['contents_add'] = 'Додати контент';
$il['contents_save'] = 'Зберегти';
$il['contents_edit'] = 'Редагувати';
$il['contents_delete'] = 'Видалити';
$il['contents_select_action'] = 'Виберіть дію:';
$il['contents_delete_selected'] = 'Видалити вибрані';
$il['contents_delete_all'] = 'Очистити блок вмісту';
